# Copyright (C) 2009 The Android Open Source Project
# Copyright (c) 2011, The Linux Foundation. All rights reserved.
# Copyright (C) 2017-2018 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import common
import re

def FullOTA_InstallEnd(info):
  OTA_InstallEnd(info)
  return

def IncrementalOTA_InstallEnd(info):
  OTA_InstallEnd(info)
  return

def AddImage(info, directory, basename, dest):
  data = info.input_zip.read(directory + "/" + basename)
  common.ZipWriteStr(info.output_zip, basename, data)
  info.script.Print("Patching {} image unconditionally...".format(dest.split('/')[-1]))
  info.script.AppendExtra('package_extract_file("%s", "%s");' % (basename, dest))

def OTA_InstallEnd(info):
  info.script.Print("Patching firmware images...")
  AddImage(info, "IMAGES", "vbmeta.img", "/dev/block/platform/bootdevice/by-name/vbmeta")
  AddImage(info, "IMAGES", "vbmeta_system.img", "/dev/block/platform/bootdevice/by-name/vbmeta_system")
  AddImage(info, "IMAGES", "vbmeta_vendor.img", "/dev/block/platform/bootdevice/by-name/vbmeta_vendor")
  AddImage(info, "IMAGES", "dtbo.img", "/dev/block/platform/bootdevice/by-name/dtbo")
  AddImage(info, "RADIO", "lk.img", "/dev/block/platform/bootdevice/by-name/lk")
  AddImage(info, "RADIO", "lk2.img", "/dev/block/platform/bootdevice/by-name/lk2")
  AddImage(info, "RADIO", "logo.bin", "/dev/block/platform/bootdevice/by-name/logo")
  AddImage(info, "RADIO", "md1img.img", "/dev/block/platform/bootdevice/by-name/md1img")
  AddImage(info, "RADIO", "preloader_raw.bin", "/dev/block/platform/bootdevice/by-name/preloader")
  AddImage(info, "RADIO", "scp1.img", "/dev/block/platform/bootdevice/by-name/scp1")
  AddImage(info, "RADIO", "scp2.img", "/dev/block/platform/bootdevice/by-name/scp2")
  AddImage(info, "RADIO", "spmfw.img", "/dev/block/platform/bootdevice/by-name/spmfw")
  AddImage(info, "RADIO", "sspm1.img", "/dev/block/platform/bootdevice/by-name/sspm_1")
  AddImage(info, "RADIO", "sspm2.img", "/dev/block/platform/bootdevice/by-name/sspm_2")
  AddImage(info, "RADIO", "tee1.img", "/dev/block/platform/bootdevice/by-name/tee1")
  AddImage(info, "RADIO", "tee2.img", "/dev/block/platform/bootdevice/by-name/tee2")
  return
